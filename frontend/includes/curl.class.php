<?php
	// API connection class
	class call {
		private $call = false;
		private $call_url = 'http://localhost/api.php';

		public function __construct( $input_url = null ) {
			$this->call_url = ( isset( $input_url ) ? $input_url : $this->call_url );
		}

		public function status() {
			$this->connect();
			curl_setopt( $this->call, CURLOPT_URL, $this->call_url . '?status' );
			$result = curl_exec( $this->call );
			if ( $result !== false ) {
				$result = json_decode( $result );
				if ( $result->status == 'up' ) {
					return 1;
				} else {
					return 0;
				}
			} else {
				return 0;
			}
			$this->disconnect();
		}

		private function connect() {
			$this->call = curl_init();
			curl_setopt( $this->call, CURLOPT_FRESH_CONNECT, true );
			curl_setopt( $this->call, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $this->call, CURLOPT_FAILONERROR, true );
		}

		private function disconnect() {
			if ( $this->call !== false ) {
				curl_close( $this->call );
			}
		}

		public function request( $call_request = null ) {
			$this->connect();
			curl_setopt( $this->call, CURLOPT_URL, $this->call_url . $call_request );
			$result = curl_exec( $this->call );
			if ( $result !== false ) {
				return $result;
			} else {
				return json_encode( array( 'error' => curl_error( $this->call ) ) );
			}
			$this->disconnect();
		}
	}
