<?php

	// Include the config and class files
	require_once( 'includes/config.php' );
	require_once( 'includes/curl.class.php' );

	echo '<h1><a href="?">Prop</a></h1>' . PHP_EOL;

	$call = new call( API_URL );

	if ( $_GET['p'] === null ) {
		$e = json_decode( $call->request( '?get=entities&from=id-parent&using=0' ) );
		for( $i = 0; $i < count( $e ); $i++ ) {
			echo '<a href="?p=' . $e[$i]->slug . '">' . date( 'F j, Y, g:i a', $e[$i]->date ) . ': ' . $e[$i]->name . '</a>' . PHP_EOL;
		}
	} else {
		$id = $_GET['p'];
		$e = json_decode( $call->request( '?get=entities&from=slug&using=' . $id ) );
		echo '<h2>' . $e[0]->name . '</h2>' . PHP_EOL;
		echo '<p><em>' . date( 'F j, Y, g:i a', $e[0]->date ) . '</em></p>' . PHP_EOL;
		echo $e[0]->body . PHP_EOL;

		$c = json_decode( $call->request( '?get=entities&from=id-parent&using=' . $e[0]->id ) );
		for( $i = 0; $i < count( $c ); $i++ ) {
			echo '<h2>' . $c[$i]->name . '</h2>' . PHP_EOL;
			echo $c[$i]->body . PHP_EOL;
		}
	}
