<?php

	header('Content-Type: application/json');

	// Include the config and class files
	require_once ( 'includes/config.php' );
	require_once ( 'includes/database.class.php' );

	// Connect to database and establish inputs
	$database = new database( DB_NAME );
	$query = array (
		'from' => ( isset ( $_GET['from'] ) ? $_GET['from'] : null ),
		'get' => ( isset ( $_GET['get'] ) ? $_GET['get'] : null ),
		'using' => ( isset ( $_GET['using'] ) ? $_GET['using'] : null ),
		'fuzzy' => ( isset ( $_GET['fuzzy'] ) ? true : null ),
		'count' => ( isset ( $_GET['count'] ) ? true : null ),
		'status' => ( isset ( $_GET['status'] ) ? true : null ),
	);

	$error = array();

	// Test if we have something to query
	if ( $query['status'] !== null ) {
		$error[] = array( 'type' => 'status', 'message' => 'up' );
	} else {

		if ( $query['using'] === null ) {
			$error[] = array( 'type' => 'error', 'message' => 'No using method provided.' );
		}


		// Get the object_id via various methods, depending on what we are querying against
		switch ( $query['from'] ) {
			case 'id':
				$SQL_get_id = $query['using'];
				break;
			case 'id-parent':
				$SQL_get_id = 'SELECT id FROM entities WHERE parent IN ( ' . $query['using'] . ' )';
				break;
			case 'slug':
				if ( $query['fuzzy'] ) {
					$SQL_get_id = 'SELECT id FROM entities WHERE slug LIKE "%' . $query['using'] . '%"';
				} else {
					$SQL_get_id = 'SELECT id FROM entities WHERE slug = "' . $query['using'] . '"';
				}
				break;
			default:
				$error[] = array( 'type' => 'error', 'message' => 'Invalid from method' );
				break;
		}

		// Get the final result based on the object_id
		switch ( $query['get'] ) {
			case 'entities':
				$SQL = 'SELECT * FROM entities WHERE id IN ( ' . $SQL_get_id . ' );';
				break;
			default:
				$error[] = array( 'type' => 'error', 'message' => 'Invalid get method' );
		}

		if ( empty( $error ) ) {
			// Execute the SQL call and return results
			$prepare = $database->connection->prepare( $SQL );
			$prepare->execute();
			if ( $query['count'] !== null ) {
				$error[] = array( 'count' => count( $prepare->fetchAll() ) );
			} else {
				if ( count ( $prepare->fetchAll() ) > 0 ) {
					$prepare->execute();
					$results = array();
					while ( $row = $prepare->fetch( PDO::FETCH_ASSOC ) ) {
						$results[] = $row;
					}
					echo json_encode( $results );
				} else {
					$error[] = array( 'type' => 'notice', 'message'=> 'No results found.' );
				}
			}
		}
	}
	// Close the database
	$database->close();


	// Print errors
	if ( ! empty( $error[0] ) ) {
		echo json_encode( $error );
	}
