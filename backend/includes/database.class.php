<?php
	// Database access class
        class database {
                public $connection = false;
                private $database_name = 'database';

                public function __construct( $input_name = null ) {
                        if ( $this->connection === false ) {
                                $connect_name = 'sqlite:' . ( isset( $input_name ) ? $input_name : $this->database_name );
                                try {
                                        $this->connection = new PDO( $connect_name );
                                } catch ( PDOException $error ) { 
                                        print $error->getMessage();
                                }
                        }
                }

                public function status() {
                        if ( $this->connection === true ) {
                                return 1;
                        } else {
                                return 0;
                        }
                }

                public function close() {
                        if ( $this->status() == 1) {
                                $this->connection = null;
                        }
                }

        }
