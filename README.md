# Prop

A lightweight and simple multi server database handler.

Uses a simple GET API on the backend server to retrieve database information.

Uses cURL on the frontend to communicate with the backend and display the data.
